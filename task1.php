<?php
/**
 * @param string $s
 * @return bool
 */
function checkBrackets($s)
{
    if (!is_string($s)) {
        return false;
    }

    $len = strlen($s);
    $haystack = [];
    for ($i = 0; $i < $len; $i++) {
        switch ($s[$i]) {
            case '(':
                array_push($haystack, 0);
                break;
            case ')':
                if (array_pop($haystack) !== 0)
                    return false;
                break;
            case '[':
                array_push($haystack, 1);
                break;
            case ']':
                if (array_pop($haystack) !== 1)
                    return false;
                break;
            default:
                break;
        }
    }

    if (!empty($haystack)) {
        return false;
    }

    return true;
}