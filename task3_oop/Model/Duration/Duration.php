<?php

namespace Model\Duration;

class Duration
{
    /**
     * @var integer
     */
    protected $hour;

    /**
     * Duration constructor.
     * @param integer $hour
     */
    public function __construct(int $hour)
    {
        $this->hour = $hour;
    }

    /** @return int */
    public function getHour()
    {
        return $this->hour;
    }

}