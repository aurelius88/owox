<?php

namespace Model\OccupationType;

class Grammar extends \Model\OccupationType\Occupation implements \Model\OccupationType\Contract\Occupation
{
    protected $type_name = 'Grammar';

    public function getFinallyPrice()
    {
        $ratePrice = $this->rate->getTotalPrice();
        return round($ratePrice, 2);
    }

    public function getOccupationName()
    {
        return $this->type_name;
    }

    public function getLanguage()
    {
        return $this->language;
    }


}