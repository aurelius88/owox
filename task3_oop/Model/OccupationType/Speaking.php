<?php

namespace Model\OccupationType;

class Speaking extends \Model\OccupationType\Occupation implements \Model\OccupationType\Contract\Occupation
{
    protected $type_name = 'Reading';

    public function getFinallyPrice()
    {
        return round($this->rate->getTotalPrice(), 2);
    }

    public function getOccupationName()
    {
        return $this->type_name;
    }

    public function getLanguage()
    {
        return $this->language;
    }


}