<?php

namespace Model\OccupationType\Contract;

interface Occupation
{
    /** @return string */
    public function getOccupationName();

    /** @return \Model\Language\Language */
    public function getLanguage();

    /** @return float */
    public function getFinallyPrice();
}