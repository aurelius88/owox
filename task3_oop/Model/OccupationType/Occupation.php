<?php

namespace Model\OccupationType;

abstract class Occupation
{
    protected $type_name;

    protected $language;

    protected $rate;

    public function __construct(\Model\Language\Language $language, \Model\Rate\Contract\Rate $rate)
    {
        $this->language = $language;
        $this->rate = $rate;
    }
}