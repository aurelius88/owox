<?php

namespace Model\Language;

abstract class Language
{
    /** @var string */
    protected $name;

    /**
     * Language constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }
}