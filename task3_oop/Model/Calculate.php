<?php

namespace Model;

class Calculate
{
    public function calc()
    {
        $language = new \Model\Language\English('English');
        $mass = [
            new \Model\OccupationType\Grammar(
                $language,
                new Rate\FixedRate(
                    new \Model\Duration\Duration(5), 10
                )
            ),
            new \Model\OccupationType\Speaking(
                $language,
                new Rate\HourlyRate(
                    new \Model\Duration\Duration(2), 10
                )
            ),
        ];

        $total = 0;
        foreach ($mass as $item) {
            /** @var $item \Model\OccupationType\Contract\Occupation */
            $total += $item->getFinallyPrice();
        }

        return $total;
    }
}