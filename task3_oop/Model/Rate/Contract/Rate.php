<?php

namespace Model\Rate\Contract;

interface Rate
{
    /** @return string */
    public function getTypeName();

    /** @return float */
    public function getTotalPrice();
}