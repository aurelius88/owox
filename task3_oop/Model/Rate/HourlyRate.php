<?php

namespace Model\Rate;

class HourlyRate extends \Model\Rate\Rate implements \Model\Rate\Contract\Rate
{
    protected $rate_name = 'Hourly';

    public function getTypeName()
    {
        return $this->rate_name;
    }

    public function getTotalPrice()
    {
        return $this->duration->getHour() * $this->price;
    }


}