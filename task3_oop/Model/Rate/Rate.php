<?php

namespace Model\Rate;

abstract class Rate
{
    /** @var string */
    protected $rate_name;

    /** @var \Model\Duration\Duration */
    protected $duration;

    /** @var float */
    protected $price;

    /**
     * Rate constructor.
     * @param \Model\Duration\Duration $duration
     * @param float $price
     */
    public function __construct(\Model\Duration\Duration $duration, float $price)
    {
        $this->duration = $duration;
        $this->price = $price;
    }
}