<?php

namespace Model\Rate;

class FixedRate extends \Model\Rate\Rate implements \Model\Rate\Contract\Rate
{
    protected $rate_name = 'Fixed';

    public function getTypeName()
    {
        return $this->rate_name;
    }

    public function getTotalPrice()
    {
        return $this->price;
    }


}